function [firingRate,realSpikePeakLocs,firstSpikeAmpl fAHPAmpl spikeThreshold spikeHalfWidth] = ccFiringD(firstSpikeCondition,Fs,nSweeps);

%% Default input if no input is provided
if ~exist('currInjDuration', 'var')
    currInjDuration = 1000; %sets currInjDuration to 1000 as default value
end

%% File load

[fileName,pathName] = uigetfile ('*.*', 'Select the IV traces');
load(fullfile(pathName,fileName));

%nSweeps = 25; %number of recorded sweeps
lenghtSweep = 25000; %duration of sweep in points
RmSweep = zeros(lenghtSweep,nSweeps); %preallocation

%rearrange sweeps in one matrix
traces= who('Trace_*'); %identify loaded files from DAT
traceName = traces{1}; %name of a trace
startTraceName=traceName(1:10); %first part of trace name
for ind = 1:nSweeps,
    temp = eval(strcat(startTraceName,num2str(ind),'_2')); %single sweep of 700ms
    RmSweep(:,ind)= temp(1:lenghtSweep,2);
end

inputfile = RmSweep*1000; %filtered sweep traces

clear Trace_* %clear variable that are not used

%% Conditions

%firstSpikeCondition = 1; %condition for calculation of 1st spike parameters
plotting = 1; %select 1 yes and 0 for no

%% Parameters
si_ms=1/Fs; %sampling interval in ms
SpikeThreshold = 0; %spike threshold (trace value, not amplitude)

currInjSweep1 = 20; %current injected in first sweep (in pA)
currInjDelta = 20; %delta used for current steps (in pA)
sweepTime = (0:length(inputfile)-1).*si_ms; %time of each sweep (in ms)

baseTime = 50; %time of baseline in ms
minSpikeDetInterval = 2; %minimum time between spikes for detection
minSpikeDetPnts = minSpikeDetInterval * Fs; %minimum number of points between spikes for detection
burstFiringRateLength = 50; %time to calculate burst firing rate (usually 50 ms)
burstIndexThreshold = 50; %time threshold (in ms) to calculate burst index


%% Spike detection
spikePeaks = zeros(200,size(inputfile,2)); %preallocation
spikePeaksLocs = zeros(200,size(inputfile,2)); %preallocation
lastSpikeLocs = zeros(size(inputfile,2),1); %preallocation
spikeN = zeros(1,size(inputfile,2)); %preallocation 
sweepsWithSpikes = zeros(1,size(inputfile,2)); %preallocation



lastPartCurrInjTime = (baseTime+400):(baseTime+500);
lastPartCurrInjPnts = lastPartCurrInjTime .* Fs;
lastPartCurrInjTrace = inputfile (lastPartCurrInjPnts,:);
lastPartCurrInjMean = mean(lastPartCurrInjTrace,1);

    for i = 1 : size(inputfile,2);
        if lastPartCurrInjMean(i) > SpikeThreshold;
            SpikeThreshold = lastPartCurrInjMean(i) + 10;
        end
       [spikePeaks_i,spikePeaksLocs_i] = findpeaks(inputfile(1:(baseTime+currInjDuration)*Fs,i),'MinPeakHeight',SpikeThreshold, 'MinPeakDistance', minSpikeDetPnts); %finds spike peaks for each sweep
       noSpikes = isempty(spikePeaks_i); %checks if the sweep has no spikes
       if noSpikes == 0  %if there are spikes
          sweepsWithSpikes(i) = 1;     %value of vector corresponding to sweep is one 
          spikePeaks(1:length(spikePeaks_i),i) = spikePeaks_i; %allocation of spike peaks values to matrix (each sweep is a column)
          spikePeaksLocs(1:length(spikePeaksLocs_i),i) = spikePeaksLocs_i;%allocation of spike peak locations values to matrix (each sweep is a column)
          spikeN(i) = length(spikePeaks_i);
          lastSpikeLoc_i = max(spikePeaksLocs_i);
       else
           spikeN(spikePeaks_i) = 0;
          lastSpikeLoc_i = NaN;
       end
       lastSpikeLocs(i) = lastSpikeLoc_i;
    end
    
    
firstSweepWithSpikes = find(sweepsWithSpikes,1); %first sweep with spikes
firstSpikePeakLoc = spikePeaksLocs(1,firstSweepWithSpikes); %location of the first spike in the first sweep with spikes
firstSpikePeak = spikePeaks(1,firstSweepWithSpikes);
    
firingRate = spikeN ./ (currInjDuration/1000); 
    
    
lastCurrInj = (((size(inputfile,2)-1)*currInjDelta)+currInjSweep1);
current = [currInjSweep1:currInjDelta:lastCurrInj];

spikeTimes = spikePeaksLocs ./ Fs;

% get time of spike (in ms) for each sweep
realSpikePeakLocs = spikePeaksLocs; % preallocation
realSpikePeakLocs(find(spikePeaksLocs==0)) = NaN;
realSpikePeakLocs = (realSpikePeakLocs-500)/Fs;

%% last spike time
spikePeaksLocs1 = spikePeaksLocs(:,1); %spike locations for 1st sweep
spikePeaksLocs1(spikePeaksLocs1==0) = []; %remove zeros
spikePeaks1 = spikePeaks(:,1); %spike peaks for 1st sweep
spikePeaks1(spikePeaks1==0) = []; %remove zeros
spikePeaksLocsLast = spikePeaksLocs(:,end); %spike locations for last sweep
spikePeaksLocsLast(spikePeaksLocsLast==0) = []; %remove zeros
spikePeaksLast = spikePeaks(:,end); %spike peaks for last sweep
spikePeaksLast(spikePeaksLast==0) = []; %remove zeros

lastSpikeTimes = (lastSpikeLocs' ./ Fs) - baseTime; %time of the last spike in each sweep

%% Calculates PSTH for each sweep (not used)
    
%spikePeaksLocsOffset = spikePeaksLocs - basePoints; %offsets baseline for spike peak data points; 
%binnedFiringRate = zeros(currInjTime/binSize+1,size(inputfile,2)); %preallocation
%for i = 1 : size(inputfile,2);
%    [binnedFiringRate_i] = psthNoPlot(spikePeaksLocsOffset(:,i),binSize,Fs*1000,1,currInjTime*Fs);
%    binnedFiringRate(:,i) = binnedFiringRate_i;
%end

%spikeNfirst50ms = burstFiringRate ./ binSize;

%% Burst firing (initial spikes)
initialSpikesN = zeros(1,nSweeps);
for i = 1:nSweeps;
    initialSpikes_i = find(0 < spikeTimes(:,i) & spikeTimes(:,i)<(baseTime+burstFiringRateLength));
    initialSpikesN_i = length(initialSpikes_i);
    initialSpikesN(i) = initialSpikesN_i;
end

burstIndex = initialSpikesN ./ nSweeps;
burstFiringRate = initialSpikesN ./ (burstFiringRateLength./1000);

spikeTimes(spikeTimes==0)=NaN;

%% calculate first spike parameters
if firstSpikeCondition == 0,
    spikeThreshold = NaN;
    firstSpikeAmpl = NaN;
    spikeHalfWidth = NaN;
    fAHPAmpl = NaN;
    
else
     
    %calculate and plot first spike parameters   
    [firstSpikeAmpl fAHPAmpl spikeThreshold spikeHalfWidth]=firstSpikeD(Fs,firstSweepWithSpikes,inputfile, firstSpikePeak, firstSpikePeakLoc);
    
end

%% Rheobase

TwiceRheoSweep = firstSweepWithSpikes*2; %Current step 2X bigger than rheobase
[~,spikePeaksLocs] = findpeaks(inputfile(:,TwiceRheoSweep),'MinPeakHeight',SpikeThreshold, 'MinPeakDistance', minSpikeDetPnts); %finds spike peaks for each sweep

% Adaptation index
spikePeaksTimes = spikePeaksLocs / Fs;
% isi = diff(spikePeaksTimes);
% isiCV = std(isi)/mean(isi); %CV of the ISI
% first3isi = isi(1:3);
% last3isi = isi(end-2:end);
% adaptIndex = mean(first3isi)/mean(last3isi);

%Burst index at 2X Rheobase
spikePeaksTimesInBurst = spikePeaksTimes-baseTime; %duplicates spike times vector and offsets baseline
spikePeaksTimesAboveBurstIndexThreshold = find(abs(spikePeaksTimesInBurst)>burstIndexThreshold); %find spike peak times above the time threshold for burst (50 ms usually)
spikePeaksTimesInBurst(spikePeaksTimesAboveBurstIndexThreshold) = []; %removed spike times above time threshold for burst index
spikeNinBurst = length(spikePeaksTimesInBurst);
burstIndex2X = spikeNinBurst/spikeN(TwiceRheoSweep);

   
%% Plotting 
    % Plot Firing Pattern
if plotting == 1; %if plotting is set on 'yes'
    
        %close all;
        figure('Position', [100, 100, 1049, 895]);


        subplot(3,3,1); xlim([0 1500]), ylim([-0.08 +0.05])
        plot(sweepTime,inputfile(:,1));
        hold;
        scatter(sweepTime(spikePeaksLocs1),spikePeaks1,50,'r');
        ylabel('Membrane potential (mV)');
        xlabel('Time (ms)');
        title('Spike detection 1st sweep');

        subplot(3,3,2);  xlim([0 1500]), ylim([-0.08 +0.05])
        plot(sweepTime,inputfile(:,end));
        hold;
        scatter(sweepTime(spikePeaksLocsLast),spikePeaksLast,50,'r');
        ylabel('Membrane potential (mV)');
        xlabel('Time (ms)');
        title('Spike detection last sweep');
        
        subplot(3,3,3); xlim([0 currInjDuration]), ylim([0 nSweeps])
        hold;
        for i = 1:nSweeps;
        subplot(3,3,3);
        plot (spikeTimes(:,i), i,'*b');
        end
        ylabel('Sweep n');
        xlabel('Time');
        title('Raster plot');


        subplot(3,3,4);
        plot (current,firingRate,'-ob');
        title('f/I curve')
        ylabel('Firing rate (Spikes/s)')
        xlabel('Injected current (pA)')
        xlim([0 max(current)]);
        
        subplot(3,3,5);
        plot (current,burstFiringRate,'-or');
        title('f/I curve for first 50 ms')
        ylabel('Firing rate (Spikes/s)')
        xlabel('Injected current (pA)')
        xlim([0 max(current)]);
                       
        subplot(3,3,6);
        plot (current,lastSpikeTimes,'-ob');
        title('Time of the last spike')
        ylabel('Time of the last spike (ms)')
        xlabel('Injected current (pA)')
        xlim([0 max(current)]);
        
        subplot(3,3,8);
        plot (current,burstIndex,'-or');
        title('Burst index')
        ylabel('Fraction of spikes in the first 50 ms')
        xlabel('Injected current (pA)')
        xlim([0 max(current)]);
        
               
    
            
end

% Printing results
RheoDisplay = ['2X Rheobase = ', num2str(TwiceRheoSweep*20), ' pA'];
disp (RheoDisplay);
%isiCvDisplay = ['isi CV = ', num2str(isiCV)];
%disp (isiCvDisplay);
% AdaptIndexDisplay = ['Adaptation Index = ', num2str(adaptIndex)];
% disp (AdaptIndexDisplay);
BurstIndexDisplay = ['Burst Index = ', num2str(burstIndex2X)];
disp (BurstIndexDisplay);

        
end

  
