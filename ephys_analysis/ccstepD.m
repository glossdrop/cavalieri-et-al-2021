function [inputRes membraneTau membraneCapacitance ccstepFirstSpike fileName] = ccstepD(Fs, K)
    
%% File loading

[fileName,pathName] = uigetfile ('*.*', 'Select the IV traces');

load(fullfile(pathName,fileName));

nSweeps = 5; %number of recorded sweeps
%TimeVector = (Trace_1_2_1_2(1:8000,1))*1000; %create time vector for each sweep
RmSweep = zeros(8000,nSweeps*2); %empty matrix for sweeps
order = [5,6;4,7;3,8;2,9;1,10]; %new order for matrix

%rearrange sweeps in one matrix
traces= who('Trace_*'); %identify loaded files from DAT
traceName = traces{1}; %name of a trace
startTraceName=traceName(1:10); %first part of trace name

%rearrange sweeps from pairs to 1 by 1 in the matrix
for ind = 1:nSweeps,
    temp = eval(strcat(startTraceName,num2str(ind),'_2')); %single sweep of 700ms
    new_ind1 = order(ind,1);
    RmSweep(:,new_ind1)= temp(1:8000,2); %hyperpol sweep
    new_ind2 = order(ind,2);
    RmSweep(:,new_ind2)= temp(15001:end,2); %depol sweep
end

%inputfile = sgolayfilt(RmSweep,3,125)*1000; %filtered sweep traces
inputfile = RmSweep*1000; %filtered sweep 

%% Parameters
plotting = 1; %select 1 yes and 0 for no

si_ms=1/Fs; %sampling interval in ms
base = 180; % baseline time (in ms)
current = [-100,-80,-60,-40,-20,10,20,30,40,50]; %current steps in new order
currTime = 500; %length of depolarizing/hyperpolarizing current injections (in ms)
voltageRange = 100;  %range that is averaged to measure membrane potential at the end of each current injection (in ms)
voltageEndTime = 50; %ms before the end of the current step in which membrane potential is measured (usually set to 50 ms)
voltageEndPnts = voltageEndTime*Fs;
%SpikeThreshold = -20; % spike threshold (trace value, not amplitude) 
Threshold = 0;
tauSweepN = 5; %sweep with -20pA current injection to calculate tau DAVIDE
tauSweepLength = 100; %length of the sweep to use to fit exponential for tau (in ms) DAVIDE
sweepTime = (0:length(inputfile)-1).*si_ms; %time of each sweep (in ms)


%% Conversion to mV (if data are in V)
% if conversion == 0
% else
%     inputfile = inputfile*1000;
% end
% 

%% IV for Input resistance
sweepsWithSpikes = zeros(1,size(inputfile,2)); %preallocation

%% Spike detection
spikePeaks = zeros(200,size(inputfile,2)); %preallocation
spikePeaksLocs = zeros(200,size(inputfile,2)); %preallocation

for i = 1 : size(inputfile,2);
   [spikePeaks_i,spikePeaksLocs_i] = findpeaks(inputfile(:,i),'MinPeakHeight',Threshold); %spike peak detection in every sweep
   noSpikes = isempty(spikePeaks_i); %checks if the sweep has no spikes
   if noSpikes == 0;  %if there are spikes
      sweepsWithSpikes(i) = 1;     %value of vector corresponding to sweep is one
      spikePeaks(1:length(spikePeaks_i),i) = spikePeaks_i; %allocation of spike peaks values to matrix (each sweep is a column)
      spikePeaksLocs(1:length(spikePeaksLocs_i),i) = spikePeaksLocs_i;%allocation of spike peak locations values to matrix (each sweep is a column)
   else

   end

end

firstSweepWithSpikes = find(sweepsWithSpikes,1); %first sweep with spikes
firstSpikePeakLoc = spikePeaksLocs(1,firstSweepWithSpikes); %location of the first spike in the first sweep with spikes
firstSpikePeak = spikePeaks(1,firstSweepWithSpikes);

%%
ccstepFirstSpike=NaN(4,1);
if sweepsWithSpikes == 0, %no spikes
    warning('WARNING: NO SPIKES DETECTED'); %#ok<WNTAG>


else % Analysis of the first spikes
   [ccstepFirstSpike(1) ccstepFirstSpike(2) ccstepFirstSpike(3) ccstepFirstSpike(4)] ...
       = firstSpikeD(Fs,firstSweepWithSpikes ,inputfile, firstSpikePeak, firstSpikePeakLoc);
end
   
    %% Matrices 
if sweepsWithSpikes == 0;
    inputfileIV = sgolayfilt(inputfile,3,125);
    else
    inputfileIV = sgolayfilt(inputfile(:,1:firstSweepWithSpikes-1),3,125);
    current = current(:,1:firstSweepWithSpikes-1);
end
 
    
%% Input Resistance

basePnts = base * Fs;
currPnts = currTime * Fs;
avgPnts = voltageRange * Fs;
avgStartPnts = currPnts - voltageEndPnts; %point in the trace where measurement of membrane potential starts
baseV = median(inputfileIV(1:basePnts,:)); %baseline membrane potentials
V = median(inputfileIV(avgStartPnts:avgStartPnts+avgPnts,:)); %membrane potentials at the end of each current step
deltaV = V - baseV; %vector with delta membrane potentials    
deltaV(find(current==0)) = 0;
inputRes = (deltaV*1000/current); %input resistance in M? (slope of the I/V fit with intercept forced through zero)  
    
    
%% tau & Membrane capacitance

tauSweep = inputfileIV(:,tauSweepN);%create vector with sweep to calculate tau (usually -20pA)
startCut=1990; %empirically works
tauSweepCut = tauSweep(startCut:startCut+tauSweepLength*Fs); %sweep to calculate tau limited to current injection
timeTauSweepCut = [1:length(tauSweepCut)]'./Fs; %time vector

F = @(k,t) k(1)*exp(-t*k(2)) + k(3)*exp(-t*k(4))+k(5); %biexponential
[k,~,~,~,~] = lsqcurvefit(F,K,timeTauSweepCut,tauSweepCut);
fitted = k(1)*exp(-timeTauSweepCut*k(2)) + k(3)*exp(-timeTauSweepCut*k(4))+k(5);

membraneTau = max([1/k(2) 1/k(4)]); %finds tau with highest values from the fit 

membraneCapacitance = (membraneTau/inputRes)*1000; %membrane capacitance in pF


%
   if plotting == 1;     
        
        % Plotting
        %close all
        figure('Position', [100, 100, 1024, 640]);


            % IV
            subplot(2,2,1)
            plot(sweepTime,inputfileIV)
            xlim([0 max(sweepTime)])
            title('Sweeps used for IV')
            ylabel('Membrane potential (mV)')
            xlabel('time (ms)')


            subplot(2,2,2); 
            scatter(current,deltaV), hold on, plot([0 0], ylim,'k--'),plot(xlim,[0 0], 'k--')
            title('I-V curve')
            xlabel('Current (pA)')
            ylabel('Delta Vm (mV)')
            
            % tau
            subplot(2,2,3);
            plot(sweepTime, tauSweep,'k'),
            hold on, plot(timeTauSweepCut+startCut/Fs,fitted)%, ylim([-80 -60])
            xlim([200 300])
            title('Membrane tau fit')
            ylabel('Membrane potential (mV)')
            xlabel('time (ms)')
            

            
   else
       
   end

    inputResDisplay = ['Rin = ', num2str(inputRes), ' MOhm'];
    disp (inputResDisplay);
    membraneTauDisplay = ['Membrane tau = ', num2str(membraneTau), ' ms'];
    disp (membraneTauDisplay);
    membraneCapacitanceDisplay = ['Membrane capacitance = ', num2str(membraneCapacitance), ' pF'];
    disp (membraneCapacitanceDisplay);
        
end

